FROM alpine

RUN apk --no-cache add bash curl; \
    curl -H 'Cache-Control: no-cache' -sS https://raw.githubusercontent.com/fossas/fossa-cli/master/install-latest.sh | bash;

COPY pipe.sh /

RUN chmod a+x /pipe.sh

ENTRYPOINT ["/pipe.sh"]
