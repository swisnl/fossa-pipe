#!/usr/bin/env bash

set -e

# Functions.
init_array_var() {
    local array_var=${1}
    local count_var=${array_var}_COUNT
    for (( i = 0; i < ${!count_var:=0}; i++ ))
    do
      eval ${array_var}[$i]='$'${array_var}_${i}
    done
}

# Required parameters
API_KEY=${API_KEY:?'API_KEY environment variable missing.'}
COMMAND=${COMMAND:?'COMMAND environment variable missing.'}

# Optional parameters
init_array_var "PARAMETERS"

# Run FOSSA command
if [ -z "${OUTPUT}" ]
then
  FOSSA_API_KEY="${API_KEY}" fossa "${COMMAND}" "${PARAMETERS[@]}"
else
  FOSSA_API_KEY="${API_KEY}" fossa "${COMMAND}" "${PARAMETERS[@]}" > "${OUTPUT}"
fi
