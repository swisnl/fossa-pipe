# Bitbucket Pipelines Pipe: FOSSA

This pipe is a wrapper around [FOSSA CLI](https://github.com/fossas/fossa-cli) to have your project scanned with [FOSSA](https://fossa.com/). It includes everything that's required to analyse your PHP and JavaScript projects.

## YAML Definition

Add the following snippet to the script section of your `bitbucket-pipelines.yml` file:

```yaml
- pipe: swisnl/fossa-pipe:3
  variables:
    API_KEY: '<string>'
    COMMAND: '<string>'
    # PARAMETERS: ['<string>'] # Optional.
    # OUTPUT: ['<string>'] # Optional.
```

## Variables

| Variable    | Usage                                                                                                                                    |
| ----------- |------------------------------------------------------------------------------------------------------------------------------------------|
| API_KEY (*) | Your FOSSA API token. See [prerequisites](#markdown-header-prerequisites).                                                               |
| COMMAND (*) | The desired FOSSA CLI command. See [CLI Reference](https://github.com/fossas/fossa-cli/blob/master/docs/README.md#references). |
| PARAMETERS  | Optional parameters for the FOSSA CLI command.                                                                                           |
| OUTPUT      | Where to redirect the output of the command.                                                                                             |

_(*) = required variable._

## Prerequisites

To use this pipe, you need a FOSSA API token, and preferably a configuration file. To create a token, visit your [Account Settings](https://app.fossa.com/account/settings/integrations/api_tokens). Please see the [example config file](.fossa.example.yml) or refer to the [user guide](https://github.com/fossas/fossa-cli/blob/master/docs/README.md) to learn how to configure your project. Last, all your dependencies must be present (installed).

## Examples

Basic example:

```yaml
script:
  # Your own install steps
  - pipe: swisnl/fossa-pipe:3
    variables:
      API_KEY: $FOSSA_API_KEY
      COMMAND: analyze
  # Your own build steps
  - pipe: swisnl/fossa-pipe:3
    variables:
      API_KEY: $FOSSA_API_KEY
      COMMAND: test
```

Advanced example:

```yaml
script:
  # Your own install steps
  - pipe: swisnl/fossa-pipe:3
    variables:
      API_KEY: $FOSSA_API_KEY
      COMMAND: analyze
      PARAMETERS: ["--config", "/path/to/your/.fossa.yml"]
  # Your own build steps
  - pipe: swisnl/fossa-pipe:3
    variables:
      API_KEY: $FOSSA_API_KEY
      COMMAND: test
      PARAMETERS: ["--config", "/path/to/your/.fossa.yml", "--suppress-issues"]
  - pipe: swisnl/fossa-pipe:3
    variables:
      API_KEY: $FOSSA_API_KEY
      COMMAND: report
      PARAMETERS: ["licenses"]
      OUTPUT: NOTICE.md
```

## Support

If you’d like help with this pipe, or you have an issue or feature request, let us know. The pipe is maintained by info@swis.nl.

If you're reporting an issue, please include:

* the version of the pipe
* relevant logs and error messages
* steps to reproduce

## Legal

This pipe is not affiliated with nor endorsed by FOSSA. FOSSA is a registered trademark of FOSSA Inc.
